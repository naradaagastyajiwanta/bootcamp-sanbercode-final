import React from "react";
import { createContext } from "react";
import { useState } from "react";
import axios from "axios";
import { useEffect } from "react";

export const GlobalContext = createContext();

export const GlobalProvider = ({ children }) => {
  const [data, setdata] = useState([]);
  const [loading, setLoading] = useState(false);
  const [edit, setEdit] = useState(false);

  const fetchData = async () => {
    setLoading(true);

    try {
      const response = await axios.get(
        "https://arhandev.xyz/public/api/final/products"
      );
      console.log(response);
      setdata(response.data.data);
      console.log(data);
      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  return (
    <GlobalContext.Provider
      value={{
        data: data,
        setdata: setdata,
        loading: loading,
        setLoading: setLoading,
        edit: edit,
        setEdit: setEdit,
        fetchData: fetchData,
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};

export default GlobalContext;
