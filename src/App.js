import "./App.css";
import LayoutMain from "./Components/LayoutMain";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Home from "./Pages/Home";
import Produk from "./Pages/Produk";
import SettingProduk from "./Pages/SettingProduk";
import BuatProduk from "./Pages/BuatProduk";
import Login from "./Pages/Login";
import Register from "./Pages/Register";
import AuthenticatedRoutes from "./reusable/AuthenticatedRoutes";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    element: <AuthenticatedRoutes />,
    children: [
      {
        path: "/settingproduk",
        element: <SettingProduk />,
      },
      {
        path: "/buatproduk",
        element: <BuatProduk />,
      },
    ],
  },
  {
    path: "/produk",
    element: <Produk />,
  },

  {
    path: "/login",
    element: <Login />,
  },
  {
    path: "/register",
    element: <Register />,
  },
]);

function App() {
  return (
    <div className="App">
      <LayoutMain>
        <RouterProvider router={router} />
      </LayoutMain>
    </div>
  );
}

export default App;
