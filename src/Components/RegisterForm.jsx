import { useState } from "react";
import axios from "axios";
import { useEffect, useContext } from "react";
import "../Style/BuatForm.css";
import GlobalContext from "../Context/GlobalContext";
import "../Style/BuatForm.css";

function RegisterForm({ children }) {
  const { fetchData, data, loading, setLoading } = useContext(GlobalContext);

  const [showhide, setshowhide] = useState(false);
  const [Input, setInput] = useState({
    email: "",
    nama: "",
    username: "",
    password: "",
    password_confirmation: "",
  });

  const handleType = (event) => {
    if (event.target.name === "email") {
      setInput({ ...Input, email: event.target.value });
    } else if (event.target.name === "nama") {
      setInput({ ...Input, nama: event.target.value });
    } else if (event.target.name === "username") {
      setInput({ ...Input, username: event.target.value });
    } else if (event.target.name === "password") {
      setInput({ ...Input, password: event.target.value });
    } else if (event.target.name === "password_confirmation") {
      setInput({ ...Input, password_confirmation: event.target.value });
    }
  };

  const handleSubmit = async () => {
    console.log(Input);
    setLoading(true);

    try {
      const response = await axios.post(
        "https://arhandev.xyz/public/api/final/register",
        {
          email: Input.email,
          nama: Input.nama,
          username: Input.username,
          password: Input.password,
          password_confirmation: Input.password_confirmation,
        }
      );

      setInput({
        email: "",
        nama: "",
        username: "",
        password: "",
        password_confirmation: "",
      });
      fetchData();
      setLoading(false);
      alert("Berhasil Register");
      console.log(response);
    } catch (error) {
      /*alert(error.response.data.info);*/
      console.log(error);
      setLoading(false);
    }
  };

  useEffect(() => {}, [handleSubmit]);

  return (
    <div class="container">
      <div class="card">
        <h1 class="header">{children}</h1>
        <div class="input-container">
          <div class="input-group">
            <label>Email: </label>
            <input
              class="input-kolom"
              type="text"
              value={Input.email}
              placeholder="Masukan Email Anda"
              onChange={handleType}
              name="email"
            />
          </div>
          <div class="input-group">
            <label>Nama : </label>
            <input
              class="input-kolom"
              type="text"
              value={Input.nama}
              placeholder="Masukan Nama Lengkap"
              onChange={handleType}
              name="nama"
            />
          </div>
          <div class="input-group">
            <label>Username : </label>
            <input
              class="input-kolom"
              type="text"
              placeholder="Masukan Username"
              value={Input.username}
              onChange={handleType}
              name="username"
            ></input>
          </div>
          <div class="input-group">
            <label>Password: </label>
            <input
              class="input-kolom"
              type="text"
              placeholder="Masukan Password"
              value={Input.password}
              onChange={handleType}
              name="password"
            ></input>
          </div>
          <div class="input-group">
            <label>Password Confirmation: </label>
            <input
              class="input-kolom"
              type="text"
              placeholder="Masukan Password"
              value={Input.password_confirmation}
              onChange={handleType}
              name="password_confirmation"
            ></input>
          </div>
        </div>
        <div class="btn-submit-container">
          <button
            class="button-submit"
            type="button"
            disabled={loading}
            onClick={handleSubmit}
          >
            {loading === true ? "Loading..." : "Simpan"}
          </button>
        </div>
      </div>
    </div>
  );
}

export default RegisterForm;
