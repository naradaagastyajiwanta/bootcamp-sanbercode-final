import { useState } from "react";
import axios from "axios";
import { useEffect, useContext } from "react";
import "../Style/BuatForm.css";
import GlobalContext from "../Context/GlobalContext";
import "../Style/LoginForm.css";
import { Link, useNavigate } from "react-router-dom";

function LoginForm({ children }) {
  const { fetchData, data, loading, setLoading } = useContext(GlobalContext);
  const [showhide, setshowhide] = useState(false);
  const [Input, setInput] = useState({
    email: "",
    password: "",
  });
  const navigate = useNavigate();

  const handleType = (event) => {
    if (event.target.name === "email") {
      setInput({ ...Input, email: event.target.value });
    } else if (event.target.name === "password") {
      setInput({ ...Input, password: event.target.value });
    }
  };

  const handleSubmit = async () => {
    console.log(Input);
    setLoading(true);

    try {
      const response = await axios.post(
        "https://arhandev.xyz/public/api/final/login",
        {
          email: Input.email,
          password: Input.password,
        }
      );
      localStorage.setItem("token", response.data.data.token);
      localStorage.setItem("username", response.data.data.user.username);
      localStorage.setItem("name", response.data.data.user.name);

      setInput({
        email: "",
        password: "",
      });
      setLoading(false);
      alert("Berhasil Login");
      console.log(response.data.token);
      fetchData();
      navigate("/");
    } catch (error) {
      /*alert(error.response.data.info);*/
      console.log(error);
      setLoading(false);
    }
  };

  useEffect(() => {}, [handleSubmit]);

  return (
    <div class="container">
      <div class="card">
        <h1 class="header">{children}</h1>
        <div class="input-container">
          <div class="input-group">
            <label>Email: </label>
            <input
              class="input-kolom"
              type="text"
              value={Input.email}
              placeholder="Masukan Email Anda"
              onChange={handleType}
              name="email"
            />
          </div>

          <div class="input-group">
            <label>Password: </label>
            <input
              class="input-kolom"
              type="text"
              placeholder="Masukan Password"
              value={Input.password}
              onChange={handleType}
              name="password"
            ></input>
          </div>
        </div>
        <div class="btn-submit-container">
          <button
            class="button-submit"
            type="button"
            disabled={loading}
            onClick={handleSubmit}
          >
            {loading === true ? "Loading..." : "Login"}
          </button>
        </div>
      </div>
      <div>
        Belum punya akun?
        <Link to="/register">
          <button className="btn-register">Register</button>
        </Link>
      </div>
    </div>
  );
}

export default LoginForm;
