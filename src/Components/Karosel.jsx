import { React, useEffect, useContext } from "react";
import { GlobalContext, GlobalProvider } from "../Context/GlobalContext";
import { Carousel } from "antd";
import "../Style/Karosel.css";

const contentStyle = {
  height: "760px",
  color: "#fff",

  background: "#364d79",
};

function Karosel() {
  return (
    <Carousel autoplay>
      <div className="karoseldiv">
        <h3 className="karosel" style={contentStyle}>
          {" "}
          <h1 className="h1-karosel">Produk Baru!</h1>
          <img
            className="img-karosel"
            src="https://www.jd.id/news/wp-content/uploads/2020/11/2f93506b003cc13f3eac93ec785c6e61_review-asus-rog-strix-g15-g512l-9.jpg"
            alt=""
          />
        </h3>
      </div>
      <div className="karosel">
        <h3 className="karosel" style={contentStyle}>
          <h1 className="h1-karosel">Produk Keren!</h1>
          <img
            className="img-karosel"
            src="https://www.yangcanggih.com/wp-content/uploads/2022/05/ASUS-ROG-Strix-SCAR-17-Special-Edition-1.jpg"
            alt=""
          />
        </h3>
      </div>
    </Carousel>
  );
}

export default Karosel;
