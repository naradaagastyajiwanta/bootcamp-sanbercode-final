import React, { useContext } from "react";
import { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate, useParams } from "react-router-dom";
import GlobalContext from "../Context/GlobalContext";
import { Select, Input as Ainput, Checkbox } from "antd";

function EditForm() {
  const { fetchData, setEdit, edit, data, loading, setLoading } =
    useContext(GlobalContext);
  const [showhide, setshowhide] = useState(false);

  const [Input, setInput] = useState({
    nama: "",
    harga: "",
    harga_diskon: null,
    is_diskon: 0,
    stock: "",
    description: "",
    category: "",
    image_url: "",
  });
  const { id } = useParams();
  console.log(id);
  const navigate = useNavigate();
  const fetchDataEdit = () => {
    axios
      .get(`https://arhandev.xyz/public/api/final/products/${id}`)
      .then((result) => {
        console.log(result);
        const product = result.data.data;
        setInput(product);
      })
      .catch((err) => {
        console.error(err);
      });
  };

  console.log(Input);
  const onChange = (value) => {
    console.log(`selected ${value}`);
    setInput({ ...Input, category: value });
  };
  const onSearch = (value) => {
    console.log("search:", value);
  };
  const handleType = (event) => {
    if (event.target.name === "nama") {
      setInput({ ...Input, nama: event.target.value });
    } else if (event.target.name === "harga") {
      setInput({ ...Input, harga: event.target.value });
    } else if (event.target.name === "is_diskon") {
      setInput({ ...Input, is_diskon: event.target.value });
    } else if (event.target.name === "stock") {
      setInput({ ...Input, stock: event.target.value });
    } else if (event.target.name === "image_url") {
      setInput({ ...Input, image_url: event.target.value });
    } else if (event.target.name === "harga_diskon") {
      setInput({ ...Input, harga_diskon: event.target.value });
    } else if (event.target.name === "description") {
      setInput({ ...Input, description: event.target.value });
      console.log(Input.description);
    }
    // else if (event.target.name === "category") {
    //   setInput({ ...Input, category: event.target.value });
    // }
  };

  const handleSubmit = async () => {
    console.log(Input);
    setLoading(true);

    try {
      const response = await axios.put(
        `https://arhandev.xyz/public/api/products/${id}`,
        {
          nama: Input.nama,
          harga: parseInt(Input.harga),
          stock: parseInt(Input.stock),
          is_diskon: Input.is_diskon,
          harga_diskon: parseInt(Input.harga_diskon),
          description: Input.description,
          category: Input.category,
          image_url: Input.image_url,
        }
      );
      // setInput({
      //   nama: "",
      //   harga: "",
      //   harga_diskon: null,
      //   is_diskon: "",
      //   stock: "",
      //   image_url: "",
      // });
      setEdit({});
      fetchData();
      alert("Berhasil Update");
      console.log(response);
      navigate("/settingproduk");
    } catch (error) {
      /*alert(error.response.data.info);*/
      console.log(error);
      setLoading(false);
    }
  };

  const diskon = (event) => {
    event.target.checked
      ? setInput({ ...Input, is_diskon: 1 })
      : setInput({ ...Input, is_diskon: 0 });
  };

  const handlediskon = (value) => {
    return !value;
  };

  useEffect(() => {
    fetchDataEdit();
    if (edit.is_diskon === 1) {
      setshowhide(true);
    }
    setInput({
      nama: edit.nama,
      harga: edit.harga,
      harga_diskon: edit.harga_diskon,
      is_diskon: edit.is_diskon,
      stock: edit.stock,
      description: edit.description,
      category: edit.category,
      image_url: edit.image_url,
    });
  }, [edit]);

  return (
    <div className="container">
      <div className="card">
        <h1 className="header">Update Postingan</h1>
        <div className="input-container">
          <div className="input-group">
            <label>Nama Barang: </label>
            <Ainput
              className="input-kolom"
              type="text"
              value={Input.nama}
              placeholder="Masukan Nama Barang"
              onChange={handleType}
              name="nama"
            />
          </div>
          <div className="input-group">
            <label>Harga Barang: </label>
            <Ainput
              className="input-kolom"
              type="number"
              value={Input.harga}
              placeholder="Masukan Harga Barang"
              onChange={handleType}
              name="harga"
            />
          </div>
          <div className="input-group">
            <label>Aktifkan Fitur Diskon:</label>

            <Checkbox
              className="input-kolom-cek"
              type="checkbox"
              value={Input.is_diskon}
              onChange={() => {
                setshowhide(handlediskon);
              }}
              onClick={diskon}
              name="is_diskon"
              checked={Input.is_diskon === 1}
            />
          </div>

          {showhide && (
            <div className="input-group">
              <label>Harga Diskon: </label>
              <Ainput
                className="input-kolom"
                type="number"
                value={Input.harga_diskon}
                placeholder="Masukan Harga setelah Diskon"
                id="harga-diskon"
                onChange={handleType}
                name="harga_diskon"
              />
            </div>
          )}
          <div className="input-group">
            <label>Stok Barang: </label>
            <Ainput
              className="input-kolom"
              type="number"
              placeholder="Masukan Jumlah Stok Barang"
              value={Input.stock}
              onChange={handleType}
              name="stock"
            />
          </div>
          <div className="input-group">
            <label>Deskripso Barang: </label>
            <Ainput
              className="input-kolom"
              type="text"
              placeholder="Deskripsokan Produkmu"
              value={Input.description}
              onChange={handleType}
              name="description"
            ></Ainput>
          </div>
          <div className="input-group">
            <label>category Barang: </label>
            <Select
              showSearch
              name="category"
              id="category"
              placeholder={Input.category}
              optionFilterProp="children"
              onChange={onChange}
              onSearch={onSearch}
              filterOption={(input, option) =>
                (option?.label ?? "")
                  .toLowerCase()
                  .includes(input.toLowerCase())
              }
              options={[
                {
                  value: "makanan",
                  label: "Makanan",
                },
                {
                  value: "teknologi",
                  label: "Teknologi",
                },
                {
                  value: "minuman",
                  label: "Minuman",
                },
                {
                  value: "lainnya",
                  label: "Lainnya",
                },
              ]}
            />
          </div>
          <div className="input-group">
            <label>Image Url: </label>
            <Ainput
              className="input-kolom"
              type="text"
              value={Input.image_url}
              placeholder="Masukan URL Gambar"
              onChange={handleType}
              name="image_url"
            />
          </div>
        </div>
        <div className="btn-submit-container">
          <button
            className="button-submit"
            type="button"
            disabled={loading}
            onClick={handleSubmit}
          >
            {loading === true ? "Loading..." : "Simpan"}
          </button>
        </div>
      </div>
    </div>
  );
}

export default EditForm;
