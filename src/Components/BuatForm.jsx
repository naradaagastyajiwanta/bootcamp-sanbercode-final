import { useState } from "react";
import axios from "axios";
import { useEffect, useContext } from "react";
import "../Style/BuatForm.css";
import GlobalContext from "../Context/GlobalContext";
import "../Style/BuatForm.css";
import { Select, Input as Ainput, Checkbox } from "antd";

function BuatForm() {
  const { fetchData, data, loading, setLoading } = useContext(GlobalContext);
  const [showhide, setshowhide] = useState(false);
  const [Input, setInput] = useState({
    nama: "",
    harga: "",
    harga_diskon: null,
    is_diskon: 0,
    stock: "",
    deskripsi: "",
    category: "",
    image_url: "",
  });
  const onChange = (value) => {
    console.log(`selected ${value}`);
    setInput({ ...Input, category: value });
  };
  const onSearch = (value) => {
    console.log("search:", value);
  };
  const handleType = (event) => {
    if (event.target.name === "nama") {
      setInput({ ...Input, nama: event.target.value });
    } else if (event.target.name === "harga") {
      setInput({ ...Input, harga: event.target.value });
    } else if (event.target.name === "is_diskon") {
      setInput({ ...Input, is_diskon: event.target.value });
    } else if (event.target.name === "stock") {
      setInput({ ...Input, stock: event.target.value });
    } else if (event.target.name === "image_url") {
      setInput({ ...Input, image_url: event.target.value });
    } else if (event.target.name === "harga_diskon") {
      setInput({ ...Input, harga_diskon: event.target.value });
    } else if (event.target.name === "deskripsi") {
      setInput({ ...Input, deskripsi: event.target.value });
    }
    // else if (event.target.name === "category") {
    //   setInput({ ...Input, category: event.target.value });
    // }
  };

  const handleSubmit = async () => {
    console.log(Input);
    setLoading(true);
    const token = localStorage.getItem("token");
    try {
      const response = await axios.post(
        "https://arhandev.xyz/public/api/final/products",
        {
          nama: Input.nama,
          harga: parseInt(Input.harga),
          stock: parseInt(Input.stock),
          is_diskon: Input.is_diskon,
          harga_diskon: parseInt(Input.harga_diskon),
          description: Input.deskripsi,
          category: Input.category,
          image_url: Input.image_url,
        },
        { headers: { Authorization: `Bearer ${token}` } }
      );
      setInput({
        nama: "",
        harga: "",
        harga_diskon: null,
        is_diskon: "",
        stock: "",
        image_url: "",
      });
      fetchData();
      setLoading(false);
      alert("Berhasil Menyimpan Katalog");
      console.log(response);
    } catch (error) {
      /*alert(error.response.data.info);*/
      console.log(error);
      alert(error.response.data.info);
      setLoading(false);
    }
  };

  const diskon = (event) => {
    event.target.checked
      ? setInput({ ...Input, is_diskon: 1 })
      : setInput({ ...Input, is_diskon: 0 });
  };

  const handlediskon = (value) => {
    return !value;
  };

  useEffect(() => {}, [handleSubmit]);

  return (
    <div class="container">
      <div class="card">
        <h1 class="header">Membuat Postingan</h1>
        <div class="input-container">
          <div class="input-group">
            <label>Nama Barang: </label>
            <Ainput
              class="input-kolom"
              type="text"
              value={Input.nama}
              placeholder="Masukan Nama Barang"
              onChange={handleType}
              name="nama"
            />
          </div>
          <div class="input-group">
            <label>Harga Barang: </label>
            <Ainput
              class="input-kolom"
              type="number"
              value={Input.harga}
              placeholder="Masukan Harga Barang"
              onChange={handleType}
              name="harga"
            />
          </div>
          <div class="input-group">
            <label>Aktifkan Fitur Diskon:</label>
            <Checkbox
              class="input-kolom-cek"
              type="checkbox"
              value={Input.is_diskon}
              onChange={() => {
                setshowhide(handlediskon);
              }}
              onClick={diskon}
              name="is_diskon"
            />
          </div>
          {showhide && (
            <div class="input-group">
              <label>Harga Diskon: </label>
              <Ainput
                class="input-kolom"
                type="number"
                value={Input.harga_diskon}
                placeholder="Masukan Harga setelah Diskon"
                id="harga-diskon"
                onChange={handleType}
                name="harga_diskon"
              />
            </div>
          )}
          <div class="input-group">
            <label>Stok Barang: </label>
            <Ainput
              class="input-kolom"
              type="number"
              placeholder="Masukan Jumlah Stok Barang"
              value={Input.stock}
              onChange={handleType}
              name="stock"
            ></Ainput>
          </div>
          <div class="input-group">
            <label>Deskripsi Barang: </label>
            <Ainput
              class="input-kolom"
              type="text"
              placeholder="Deskripsikan Produkmu"
              value={Input.deskripsi}
              onChange={handleType}
              name="deskripsi"
            ></Ainput>
          </div>
          <div class="input-group">
            <label>category Barang: </label>
            <Select
              showSearch
              name="category"
              id="category"
              placeholder="Select a person"
              optionFilterProp="children"
              onChange={onChange}
              onSearch={onSearch}
              filterOption={(input, option) =>
                (option?.label ?? "")
                  .toLowerCase()
                  .includes(input.toLowerCase())
              }
              options={[
                {
                  value: "makanan",
                  label: "Makanan",
                },
                {
                  value: "teknologi",
                  label: "Teknologi",
                },
                {
                  value: "minuman",
                  label: "Minuman",
                },
                {
                  value: "lainnya",
                  label: "Lainnya",
                },
              ]}
            />
          </div>
          <div class="input-group">
            <label>Image Url: </label>
            <Ainput
              class="input-kolom"
              type="text"
              value={Input.image_url}
              placeholder="Masukan URL Gambar"
              onChange={handleType}
              name="image_url"
            />
          </div>
        </div>
        <div class="btn-submit-container">
          <button
            class="button-submit"
            type="button"
            disabled={loading}
            onClick={handleSubmit}
          >
            {loading === true ? "Loading..." : "Simpan"}
          </button>
        </div>
      </div>
    </div>
  );
}

export default BuatForm;
