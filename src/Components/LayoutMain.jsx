import React from "react";
import { Layout, Menu, theme } from "antd";
import {
  BrowserRouter as Router,
  Route,
  Link,
  Routes,
  useNavigate,
} from "react-router-dom";
import Home from "../Pages/Home";
import Produk from "../Pages/Produk";
import SettingProduk from "../Pages/SettingProduk";
import BuatProduk from "../Pages/BuatProduk";
import "../Style/LayoutMain.css";
import Login from "../Pages/Login";
import Register from "../Pages/Register";
import { DownOutlined } from "@ant-design/icons";
import { Dropdown, Space } from "antd";
import axios from "axios";
import AuthenticatedRoutes from "../reusable/AuthenticatedRoutes";
import NotFound from "../Pages/notFound";
import EditProduk from "../Pages/EditProduk";

const onChange = (value) => {
  console.log(`selected ${value}`);
};
const onSearch = (value) => {
  console.log("search:", value);
};

const { Header, Content, Footer } = Layout;

function LayoutMain({ router, children }) {
  const navigate = useNavigate();
  const onLogout = async () => {
    try {
      const token = localStorage.getItem("token");
      const response = await axios.post(
        "https://arhandev.xyz/public/api/final/logout",
        {},
        { headers: { Authorization: `Bearer ${token}` } }
      );
      localStorage.removeItem("token");
      localStorage.removeItem("username");
      localStorage.removeItem("name");
      navigate("/");
    } catch (error) {
      alert(error.response.data.info);
    }
  };
  const items = [
    {
      label: <Link to="/settingproduk">Setting Produk</Link>,
      key: "0",
    },
    {
      label: <a onClick={onLogout}>Logout</a>,
      key: "1",
    },
    {
      type: "divider",
    },
    {
      label: "3rd menu item（disabled）",
      key: "3",
      disabled: true,
    },
  ];
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  return (
    <Layout>
      {/* <Router> */}
      <Header
        style={{
          position: "sticky",
          top: 0,
          zIndex: 1,
          width: "100%",
        }}
      >
        <div
          style={{
            float: "left",
            textAlign: "center",
            padding: "0 20px",
            color: "white",
            textTransform: "uppercase",
            background: "rgba(0, 255, 255, 0.2)",
          }}
        >
          Toko Online
        </div>

        <router>
          <Menu className="Login" theme="dark" mode="">
            {localStorage.getItem("token") === null ? (
              <Menu.Item>
                <Link to="/login">Login</Link>
              </Menu.Item>
            ) : (
              <div className="logout-container">
                <Dropdown
                  menu={{
                    items,
                  }}
                >
                  <a onClick={(e) => e.preventDefault()}>
                    <Space className="text-username">
                      Hai
                      <div className="username">
                        {localStorage.getItem("name")}
                      </div>
                      <DownOutlined />
                    </Space>
                  </a>
                </Dropdown>
                {/* <Menu.Item>
                    <Link to="/logout">
                      <button className="btn-Logout">Logout</button>{" "}
                    </Link>
                  </Menu.Item> */}
              </div>
            )}
          </Menu>
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["2"]}>
            <Menu.Item>
              <Link to="/">Home</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to="/produk">Produk</Link>
            </Menu.Item>
          </Menu>
        </router>
      </Header>

      <Content
        className="site-layout"
        style={{
          padding: "0 50px",
        }}
      >
        {/* <Breadcrumb
          style={{
            margin: "16px 0",
          }}
        >
          <Breadcrumb.Item>Home</Breadcrumb.Item>
          <Breadcrumb.Item>List</Breadcrumb.Item>
          <Breadcrumb.Item>App</Breadcrumb.Item>
        </Breadcrumb> */}
        <div
          style={{
            padding: 24,
            minHeight: 1080,
            background: colorBgContainer,
          }}
        >
          <Routes>
            <Route path="/settingproduk" element={<AuthenticatedRoutes />}>
              <Route path="/settingproduk" element={<SettingProduk />}></Route>
            </Route>
            <Route path="/buatproduk" element={<AuthenticatedRoutes />}>
              <Route path="/buatproduk" element={<BuatProduk />}></Route>
            </Route>

            <Route path="/" element={<Home />}></Route>
            <Route path="/produk" element={<Produk />}></Route>

            <Route path="/login" element={<Login />}></Route>
            <Route path="/register" element={<Register />}></Route>
            <Route path="/edit/:id" element={<EditProduk />}></Route>
            <Route path="*" element={<NotFound />}></Route>
          </Routes>
        </div>
      </Content>
      <Footer
        style={{
          textAlign: "center",
        }}
      >
        By Narada Agastya Jiwanta 2022
      </Footer>
      {/* </Router> */}
    </Layout>
  );
}

export default LayoutMain;
