import { React } from "react";
import { Menu, Layout } from "antd";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

function Navbar({ router }) {
  const { Header } = Layout;
  return (
    <Router>
      <Header
        style={{
          position: "sticky",
          top: 0,
          zIndex: 1,
          width: "100%",
        }}
      >
        <div
          style={{
            float: "left",
            width: 120,
            height: 31,
            margin: "16px 24px 16px 0",
            background: "rgba(255, 255, 255, 0.2)",
          }}
        >
          {" "}
          Toko Online{" "}
        </div>
        <router>
          <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["2"]}>
            <Menu.Item>
              <Link to="/">Home</Link>
            </Menu.Item>
            <Menu.Item>
              <Link to="/produk">Produk</Link>
            </Menu.Item>
          </Menu>
        </router>
      </Header>
    </Router>
  );
}

export default Navbar;
