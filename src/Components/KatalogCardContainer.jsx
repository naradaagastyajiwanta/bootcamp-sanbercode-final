import { React, useEffect, useContext } from "react";
import KatalogCard from "./KatalogCard";
import { GlobalContext, GlobalProvider } from "../Context/GlobalContext";
import "../Style/KatalogCardContainer.css";

function KatalogCardContainer() {
  const { fetchData, data, loading } = useContext(GlobalContext);
  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className="KatalogCardContainer">
      <div className="row">
        <div className="cards">
          {data.map((item, index) => {
            return (
              <KatalogCard
                image_url={item.image_url}
                nama={item.nama}
                stock={item.stock}
                harga={item.harga}
                is_diskon={item.is_diskon}
                harga_diskon={item.harga_diskon}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
}
export default KatalogCardContainer;
