import { useEffect } from "react";
import { React, useContext } from "react";
import { GlobalContext, GlobalProvider } from "../Context/GlobalContext";

function KatalogCard(props) {
  return (
    <div class="grid-item">
      {props.is_diskon === 0 ? (
        <div class="cardkatalog">
          <img class="card-img" src={props.image_url} />
          <div class="card-content">
            <h1 class="card-header">{props.nama}</h1>
            <h2 class="card-harga">
              {" "}
              Rp. {Intl.NumberFormat().format(props.harga)}
            </h2>
            <p class="card-text"> Stok Barang: {props.stock} </p>
          </div>
        </div>
      ) : (
        <div class="cardkatalog">
          <img class="card-img" src={props.image_url} />
          <div class="card-content">
            <h1 class="card-header">{props.nama}</h1>
            <h2 class="card-harga2">
              {" "}
              Rp. {Intl.NumberFormat().format(props.harga)}
            </h2>
            <h2 class="card-hargamerah">
              {" "}
              Jadi Rp. {Intl.NumberFormat().format(props.harga_diskon)}
            </h2>
            <p class="card-text"> Stok Barang: {props.stock} </p>
          </div>
        </div>
      )}
    </div>
  );
}

export default KatalogCard;
