import React from "react";

import KatalogCardContainer from "../Components/KatalogCardContainer";

function Produk() {
  return (
    <div>
      {" "}
      <h1 className="h1-Produk">Semua Produk</h1>
      <KatalogCardContainer />
    </div>
  );
}

export default Produk;
