import React from "react";
import Karosel from "../Components/Karosel";
import KatalogCardContainer from "../Components/KatalogCardContainer";
import { Link } from "react-router-dom";

function Home() {
  return (
    <>
      <Karosel />
      <div className="wrapper">
        <div>
          <h1 className="h1-katalogProduk">Katalog Produk</h1>
        </div>

        <div className="btn-box">
          <Link to="/produk">
            <button className="btn-create">See More</button>
          </Link>
        </div>
      </div>
      <KatalogCardContainer />
    </>
  );
}

export default Home;
