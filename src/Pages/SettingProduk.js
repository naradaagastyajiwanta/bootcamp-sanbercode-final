import { React, useEffect, useContext } from "react";
import { GlobalContext, GlobalProvider } from "../Context/GlobalContext";
import { Link } from "react-router-dom";
import "../Style/SettingProduk.css";
import axios from "axios";

function SettingProduk() {
  const { fetchData, data, loading, setLoading } = useContext(GlobalContext);
  const token = localStorage.getItem("token");
  const deleteUser = async (userId) => {
    setLoading(true);
    try {
      const response = await axios.delete(
        `https://arhandev.xyz/public/api/final/products/${userId}`,
        { headers: { Authorization: `Bearer ${token}` } }
      );
      fetchData();
      alert("Berhasil Delete Produk");
      setLoading(false);
    } catch (error) {
      setLoading(false);
      console.log(error);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div>
      <div className="wrapper">
        <h1 className="h1-editproduk">Edit Produk</h1>
        <div>
          <Link to="/buatproduk">
            <button className="btn-create">Create New</button>
          </Link>
        </div>
      </div>
      <div className="body-table">
        <table>
          <tr>
            <th>Nama</th>
            <th>Preview</th>
            <th>Harga</th>
            <th>Stock</th>
            <th>Status Diskon</th>
            <th>Harga Diskon</th>
            <th>Action</th>
          </tr>
          {data.map((item, index) => (
            <tr key={index}>
              <td>{item.nama}</td>
              <td>
                <img className="image-table" src={item.image_url}></img>
              </td>
              <td>Rp. {Intl.NumberFormat().format(item.harga)}</td>
              <td>{item.stock}</td>
              <td>{item.is_diskon === 0 ? "Tidak Aktif" : "Aktif"}</td>
              <td>
                {" "}
                {item.is_diskon === 0
                  ? "-"
                  : "Rp." + Intl.NumberFormat().format(item.harga_diskon)}
              </td>
              <td>
                <div class="action">
                  <Link to={`/edit/${item.id}`}>
                    <button onClick={() => {}} className="btn-edit">
                      Edit
                    </button>
                  </Link>
                  <button
                    onClick={() => deleteUser(item.id)}
                    className="btn-delete"
                  >
                    {loading === true ? "Loading..." : "Delete"}
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </table>
      </div>
    </div>
  );
}

export default SettingProduk;
